package TCS20.JunitGeometria.T20_Calculadora_Programador.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MainView extends JFrame{
		
	private JPanel contentPane;
	public String num1 = "";
	public String num2 = "";
	public String operacion = "";
	private double resultado; 
	private boolean escribiendoPrimerOperador = true;
	public JButton btns[] = new JButton[30];
	private boolean negativo = false;
	int textFieldNumINT, decimalint;
	String textFieldNumSTRING, decimal, result, numero1, numero2;


	public MainView() {
		setTitle("Calculadora");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 700);

		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(new Color(230, 230, 230));

		final JTextField textFieldNum = new JTextField();
		textFieldNum.setBounds(10, 25, 500, 120);
		textFieldNum.setText("");
		textFieldNum.setEnabled(false);
		textFieldNum.setBackground(new Color(230, 230, 230));
		textFieldNum.setBorder(null);
		textFieldNum.setText("0");
		textFieldNum.setFont(new Font("Tahoma", Font.PLAIN, 45));
		textFieldNum.setHorizontalAlignment(JTextField.RIGHT);
		contentPane.add(textFieldNum);
		
		JPanel panelRadioBtns = new JPanel();
		panelRadioBtns.setBounds(44, 169, 137, 135);
		panelRadioBtns.setBackground(new Color(230, 230, 230));
		contentPane.add(panelRadioBtns);
		panelRadioBtns.setLayout(new GridLayout(4, 1, 5, 5));
		
		final JRadioButton radioBtns[] = new JRadioButton[4];
		ButtonGroup bgroup = new ButtonGroup();

		radioBtns[0] = new JRadioButton();
		radioBtns[0].setBackground(new Color(230, 230, 230));
		radioBtns[0].setSelected(true);
		panelRadioBtns.add(radioBtns[0]);
		bgroup.add(radioBtns[0]);
		
		radioBtns[1] = new JRadioButton();
		radioBtns[1].setBackground(new Color(230, 230, 230));
		panelRadioBtns.add(radioBtns[1]);
		bgroup.add(radioBtns[1]);

		radioBtns[2] = new JRadioButton();
		radioBtns[2].setBackground(new Color(230, 230, 230));
		panelRadioBtns.add(radioBtns[2]);
		bgroup.add(radioBtns[2]);

		radioBtns[3] = new JRadioButton();
		radioBtns[3].setBackground(new Color(230, 230, 230));
		panelRadioBtns.add(radioBtns[3]);
		bgroup.add(radioBtns[3]);

		JPanel panelBtns = new JPanel();
		panelBtns.setBounds(10, 315, 500, 335);
		contentPane.add(panelBtns);
		panelBtns.setLayout(new GridLayout(6, 5, 5, 5));
		
		final JLabel labelOperacionCompleta = new JLabel("");
		labelOperacionCompleta.setBounds(10, 11, 500, 14);
		contentPane.add(labelOperacionCompleta);
		
		JLabel labelHistorial = new JLabel("HISTORIAL");
    	labelHistorial.setBounds(625, 11, 100, 25);
    	labelHistorial.setFont(new Font("Tahoma", Font.PLAIN, 20));
    	contentPane.add(labelHistorial);
    	
    	final JPanel panelHistorial = new JPanel();
    	panelHistorial.setBounds(574, 47, 200, 603);
    	panelHistorial.setBackground(new Color(230, 230, 230));
    	contentPane.add(panelHistorial);
    	panelHistorial.setLayout(new GridLayout(15, 1, 2, 0));
    	
    	JButton eliminarHistorial = new JButton("X");
    	eliminarHistorial.setBounds(521, 627, 42, 23);
    	contentPane.add(eliminarHistorial);
    	
    	JPanel panel = new JPanel();
    	panel.setBounds(10, 169, 30, 135);
    	contentPane.add(panel);
    	panel.setLayout(new GridLayout(4, 1, 0, 0));
    	panel.setBackground(new Color(230, 230, 230));
    	
    	JLabel textHEX = new JLabel("HEX");
    	panel.add(textHEX);
    	
    	JLabel textDEC = new JLabel("DEC");
    	panel.add(textDEC);
    	
    	JLabel textOCT = new JLabel("OCT");
    	panel.add(textOCT);
    	
    	JLabel textBIN = new JLabel("BIN");
    	panel.add(textBIN);
	
		for (int i = 0; i < btns.length; i++) {
			btns[i] = new JButton();
			btns[i].setBorder(null);
			panelBtns.add(btns[i]);
		}
		
		btns[0].setText("A");
		btns[1].setText("<<");
		btns[2].setText(">>");
		btns[3].setText("CBorrar");
		btns[4].setText("←");
		btns[5].setText("B");
		btns[6].setText("(");
		btns[7].setText(")");
		btns[8].setText("%");
		btns[9].setText("/");
		btns[10].setText("C");
		btns[11].setText("7");
		btns[12].setText("8");
		btns[13].setText("9");
		btns[14].setText("x");
		btns[15].setText("D");
		btns[16].setText("4");
		btns[17].setText("5");
		btns[18].setText("6");
		btns[19].setText("-");
		btns[20].setText("E");
		btns[21].setText("1");
		btns[22].setText("2");
		btns[23].setText("3");
		btns[24].setText("+");
		btns[25].setText("F");
		btns[26].setText("+/-");
		btns[27].setText("0");
		btns[28].setText(".");
		btns[28].setEnabled(false);
		btns[29].setText("=");
		
		ActionListener alBtns = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton tmpBtn = (JButton) e.getSource();
				String btnText = tmpBtn.getText();
				
				switch (btnText) {					
					case "CBorrar":
						textFieldNum.setText("0");
	
						if (escribiendoPrimerOperador) {
							num1 = textFieldNum.getText();
						}else {
							num2 = textFieldNum.getText();
						}
						break;
						
					case "←":
						
						char num[] = new char[(textFieldNum.getText().length()-1)];
						
						for (int i = 0; i < (textFieldNum.getText().length()-1); i++) {
							num[i] = textFieldNum.getText().charAt(i);
						}
													
						textFieldNum.setText(new String(num));

						if (textFieldNum.getText().equals("")) {
							textFieldNum.setText("0");
						}
						break;
						
					case "(":
					case ")":
						textFieldNum.setText(btnText);
						break;
					
					case "/":
					case "x":;
					case "-":
					case "+":
					case "%":
					case "<<":
					case ">>":
						if (!num1.equals("")) {
							operacion = btnText;
							escribiendoPrimerOperador = false;
							textFieldNum.setText("0");
						}
						break;
						
					case "=":
						if (num1.equals("")) {
							num1 = "0";
						}
						
						switch (operacion) {
							case "/":
								resultado = Double.parseDouble(num1) / Double.parseDouble(num2);
								break;
							case "x":
								resultado = Double.parseDouble(num1) * Double.parseDouble(num2);			
								break;
							case "-":
								resultado = Double.parseDouble(num1) - Double.parseDouble(num2);
								break;
							case "+":
								resultado = Double.parseDouble(num1) + Double.parseDouble(num2);	
								break;
							case "%":
								resultado = Double.parseDouble(num1) % Double.parseDouble(num2);						
								break;
							case "2√x":
								resultado = Math.sqrt(Double.parseDouble(num1));
								break;
							case "<<":
								Double tmpResultado1 = Double.parseDouble(num1);
								for (int i = 0; i <  Double.parseDouble(num2); i++) {
									tmpResultado1 *= 2;
								}
								resultado = tmpResultado1;
								break;
							case ">>":
								Double tmpResultado2 = Double.parseDouble(num1);
								for (int i = 0; i <  Double.parseDouble(num2); i++) {
									tmpResultado2 /= 2;
								}
								resultado = tmpResultado2;
								break;
							default:
								resultado = Double.parseDouble(num1);
								break;
						}
						
						if(!num1.equals("No se puede dividir entre cero")) {
							labelOperacionCompleta.setText(numero1 + operacion + numero2);
						}else {
							labelOperacionCompleta.setText("No se puede dividir entre cero");
						}
						
						num2 = "";
						escribiendoPrimerOperador = true;
						operacion = "";						
						break;
						
					case "+/-":
						if (!negativo) {
							negativo = true;
							textFieldNum.setText("-" + textFieldNum.getText());
						}else{
							negativo = false;
							char num2[] = new char[(textFieldNum.getText().length()-1)];
							
							for (int i = 0; i < (textFieldNum.getText().length()-1); i++) {
								num2[i] = textFieldNum.getText().charAt(i+1);
							}
														
							textFieldNum.setText(new String(num2));
						}
						break;
					default:
						textFieldNum.setFont(new Font("Tahoma", Font.PLAIN, 50));
	
						if (textFieldNum.getText().equals("0") || textFieldNum.getText().equals("No se puede dividir entre cero")){
							textFieldNum.setText(btnText);
						}else {
							textFieldNum.setText(textFieldNum.getText() + btnText);
						}
						
						break;
				}
				
					
				
				if (radioBtns[0].isSelected()) { //HEX
					textFieldNumSTRING = textFieldNum.getText();
					decimalint = Integer.parseInt(textFieldNumSTRING,16);
					
					radioBtns[0].setText(textFieldNumSTRING);
					radioBtns[1].setText(Integer.toString(decimalint));
					radioBtns[2].setText(Integer.toOctalString(decimalint));
					radioBtns[3].setText(Integer.toBinaryString(decimalint));
					if (escribiendoPrimerOperador) {
						
						numero1 = textFieldNum.getText();
						int dec = Integer.parseInt(numero1,16);
						num1 = String.valueOf(dec);
					}else {
						numero2 = textFieldNum.getText();
						int dec = Integer.parseInt(numero2,16);
						num2 = String.valueOf(dec);
						
					}
					result = Integer.toHexString((int)resultado);
					if ((int) resultado != 0) {
						radioBtns[0].setText(Integer.toHexString((int)resultado));
						radioBtns[1].setText(Integer.toString((int)resultado));
						radioBtns[2].setText(Integer.toOctalString((int)resultado));
						radioBtns[3].setText(Integer.toBinaryString((int)resultado));
						numero1 = result;
						int dec = Integer.parseInt(numero1,16);
						num1 = String.valueOf(dec);
						
						
					}
					
				}
				else if (radioBtns[1].isSelected()) { //DEC
					textFieldNumINT = new Double(textFieldNum.getText()).intValue();
					
					radioBtns[0].setText(Integer.toHexString(textFieldNumINT));
					radioBtns[1].setText(Integer.toString(textFieldNumINT));
					radioBtns[2].setText(Integer.toOctalString(textFieldNumINT));
					radioBtns[3].setText(Integer.toBinaryString(textFieldNumINT));
					if (escribiendoPrimerOperador) {
						numero1 = textFieldNum.getText();
						int dec = Integer.parseInt(numero1);
						num1 = String.valueOf(dec);
					}else {
						numero2 = textFieldNum.getText();
						int dec = Integer.parseInt(numero2);
						num2 = String.valueOf(dec);
					}	
					result = Integer.toString((int)resultado);
					if ((int) resultado != 0) {
						radioBtns[0].setText(Integer.toHexString((int)resultado));
						radioBtns[1].setText(Integer.toString((int)resultado));
						radioBtns[2].setText(Integer.toOctalString((int)resultado));
						radioBtns[3].setText(Integer.toBinaryString((int)resultado));
						numero1 = result;
						int dec = Integer.parseInt(numero1);
						num1 = String.valueOf(dec);
					}
					
					
				}
				else if (radioBtns[2].isSelected()) { // OCT
					textFieldNumINT = new Double(textFieldNum.getText()).intValue();
					decimal = Integer.toString(textFieldNumINT);
					decimalint = Integer.parseInt(decimal,8);
					
					radioBtns[0].setText(Integer.toHexString(decimalint));
					radioBtns[1].setText(Integer.toString(decimalint));
					radioBtns[2].setText(Integer.toString(textFieldNumINT));
					radioBtns[3].setText(Integer.toBinaryString(decimalint));
					if (escribiendoPrimerOperador) {
						
						numero1 = textFieldNum.getText();
						int dec = Integer.parseInt(numero1,8);
						num1 = String.valueOf(dec);

					}else {
						numero2 = textFieldNum.getText();
						int dec = Integer.parseInt(numero2,8);
						num2 = String.valueOf(dec);
						
					}
					result = Integer.toOctalString((int)resultado);
					if ((int) resultado != 0) {
						radioBtns[0].setText(Integer.toHexString((int)resultado));
						radioBtns[1].setText(Integer.toString((int)resultado));
						radioBtns[2].setText(Integer.toOctalString((int)resultado));
						radioBtns[3].setText(Integer.toBinaryString((int)resultado));
						numero1 = result;
						int dec = Integer.parseInt(numero1,8);
						num1 = String.valueOf(dec);
					}
				}
				else if (radioBtns[3].isSelected()) { //BIN
					textFieldNumINT = new Double(textFieldNum.getText()).intValue();
					decimal = Integer.toString(textFieldNumINT);
					decimalint = Integer.parseInt(decimal,2);
					
					radioBtns[0].setText(Integer.toHexString(decimalint));
					radioBtns[1].setText(Integer.toString(decimalint));
					radioBtns[2].setText(Integer.toOctalString(decimalint));
					radioBtns[3].setText(Integer.toString(textFieldNumINT));
					if (escribiendoPrimerOperador) {
						numero1 = textFieldNum.getText();
						int dec = Integer.parseInt(numero1,2);
						num1 = String.valueOf(dec);
					}else {
						numero2 = textFieldNum.getText();
						int dec = Integer.parseInt(numero2,2);
						num2 = String.valueOf(dec);
						
					}
					result = Integer.toBinaryString((int)resultado);
					if ((int) resultado != 0) {
						radioBtns[0].setText(Integer.toHexString((int)resultado));
						radioBtns[1].setText(Integer.toString((int)resultado));
						radioBtns[2].setText(Integer.toOctalString((int)resultado));
						radioBtns[3].setText(Integer.toBinaryString((int)resultado));
						numero1 = result;
						int dec = Integer.parseInt(numero1,2);
						num1 = String.valueOf(dec);
					}
					
				}
				
				if (btnText == "=") {
					if (Double.toString(resultado).length() > 4 && resultado != Double.POSITIVE_INFINITY) {
						resultado = Math.round(resultado* 100.0) / 100.0;
					}
					
					if(resultado == Double.POSITIVE_INFINITY){
						textFieldNum.setFont(new Font("Tahoma", Font.PLAIN, 35));
						textFieldNum.setText("No se puede dividir entre cero");
					}else {
						JLabel tmpLabel = new JLabel(labelOperacionCompleta.getText() + " = " + result, JLabel.CENTER);
						tmpLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
						panelHistorial.add(tmpLabel);
						
						
					}
					
				textFieldNum.setText(result);	
				
				resultado = 0;
				
				}
			}
		};
		
		for (int i = 0; i < btns.length; i++) {
			if (btns[i].getText().equals("<<") || btns[i].getText().equals(">>") ||  btns[i].getText().equals("CBorrar") || btns[i].getText().equals("←") || btns[i].getText().equals("(") || btns[i].getText().equals(")") || btns[i].getText().equals("%") || btns[i].getText().equals("/") || btns[i].getText().equals("x") || btns[i].getText().equals("-") || btns[i].getText().equals("+")) {
				btns[i].setBackground( new Color(240, 240, 240));
			}else if(btns[i].getText().equals("=")) {
				btns[i].setBackground( new Color(154, 186, 219));
			}else {
				btns[i].setBackground( new Color(250, 250, 250));
			}
			
			btns[i].addActionListener(alBtns);
		}
				
		ActionListener alEliminarHistorial = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panelHistorial.removeAll();
				panelHistorial.repaint();
			}
		};
		
		eliminarHistorial.addActionListener(alEliminarHistorial);
		
		ActionListener alRadioBtns = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == radioBtns[0]) {
					btns[0].setEnabled(true);
					btns[5].setEnabled(true);
					btns[10].setEnabled(true);
					btns[15].setEnabled(true);
					btns[20].setEnabled(true);
					btns[25].setEnabled(true);
					btns[12].setEnabled(true);
					btns[13].setEnabled(true);
					btns[22].setEnabled(true);
					btns[23].setEnabled(true);
					btns[16].setEnabled(true);
					btns[17].setEnabled(true);
					btns[18].setEnabled(true);
					btns[11].setEnabled(true);
					btns[12].setEnabled(true);
					btns[13].setEnabled(true);
				
					textFieldNum.setText(""+radioBtns[0].getText());
					
				} else if(e.getSource() == radioBtns[1]){
					btns[0].setEnabled(true);
					btns[5].setEnabled(true);
					btns[10].setEnabled(true);
					btns[15].setEnabled(true);
					btns[20].setEnabled(true);
					btns[25].setEnabled(true);
					btns[12].setEnabled(true);
					btns[13].setEnabled(true);
					btns[22].setEnabled(true);
					btns[23].setEnabled(true);
					btns[16].setEnabled(true);
					btns[17].setEnabled(true);
					btns[18].setEnabled(true);
					btns[11].setEnabled(true);
					btns[12].setEnabled(true);
					btns[13].setEnabled(true);
					btns[0].setEnabled(false);
					btns[5].setEnabled(false);
					btns[10].setEnabled(false);
					btns[15].setEnabled(false);
					btns[20].setEnabled(false);
					btns[25].setEnabled(false);
					
					textFieldNum.setText(""+radioBtns[1].getText());
				} else if(e.getSource() == radioBtns[2]) {
					btns[0].setEnabled(true);
					btns[5].setEnabled(true);
					btns[10].setEnabled(true);
					btns[15].setEnabled(true);
					btns[20].setEnabled(true);
					btns[25].setEnabled(true);
					btns[12].setEnabled(true);
					btns[13].setEnabled(true);
					btns[22].setEnabled(true);
					btns[23].setEnabled(true);
					btns[16].setEnabled(true);
					btns[17].setEnabled(true);
					btns[18].setEnabled(true);
					btns[11].setEnabled(true);
					btns[12].setEnabled(true);
					btns[13].setEnabled(true);
					btns[0].setEnabled(false);
					btns[5].setEnabled(false);
					btns[10].setEnabled(false);
					btns[15].setEnabled(false);
					btns[20].setEnabled(false);
					btns[25].setEnabled(false);
					btns[12].setEnabled(false);
					btns[13].setEnabled(false);
					
					textFieldNum.setText(""+radioBtns[2].getText());
				} else if(e.getSource() == radioBtns[3]) {
					btns[0].setEnabled(true);
					btns[5].setEnabled(true);
					btns[10].setEnabled(true);
					btns[15].setEnabled(true);
					btns[20].setEnabled(true);
					btns[25].setEnabled(true);
					btns[12].setEnabled(true);
					btns[13].setEnabled(true);
					btns[22].setEnabled(true);
					btns[23].setEnabled(true);
					btns[16].setEnabled(true);
					btns[17].setEnabled(true);
					btns[18].setEnabled(true);
					btns[11].setEnabled(true);
					btns[12].setEnabled(true);
					btns[13].setEnabled(true);
					btns[0].setEnabled(false);
					btns[5].setEnabled(false);
					btns[10].setEnabled(false);
					btns[15].setEnabled(false);
					btns[20].setEnabled(false);
					btns[25].setEnabled(false);
					btns[12].setEnabled(false);
					btns[13].setEnabled(false);
					btns[22].setEnabled(false);
					btns[23].setEnabled(false);
					btns[16].setEnabled(false);
					btns[17].setEnabled(false);
					btns[18].setEnabled(false);
					btns[11].setEnabled(false);
					btns[12].setEnabled(false);
					btns[13].setEnabled(false);
					
					textFieldNum.setText(""+radioBtns[3].getText());
				}
				
			}
		};
		
		for (int i = 0; i < radioBtns.length; i++) {
			radioBtns[i].addActionListener(alRadioBtns);
		}

	}
}