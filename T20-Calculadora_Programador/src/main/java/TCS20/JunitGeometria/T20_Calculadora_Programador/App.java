package TCS20.JunitGeometria.T20_Calculadora_Programador;

import TCS20.JunitGeometria.T20_Calculadora_Programador.view.MainView;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	MainView view = new MainView();
		view.setVisible(true);
	}
}
