package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class AplicacionGrafica extends JFrame{
		
	private JPanel contentPane;
	public String num1 = "";
	public String num2 = "";
	public String operacion = "";
	private boolean escribiendoPrimerOperador = true;
	public JButton btns[] = new JButton[35];
	private boolean negativo = false, second = false;
	

	public AplicacionGrafica() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(AplicacionGrafica.class.getResource("/views/calculadora.PNG")));
		setTitle("Calculadora");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);

		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(new Color(230, 230, 230));

		final JTextField textFieldNum = new JTextField();
		textFieldNum.setBounds(10, 25, 500, 150);
		textFieldNum.setText("");
		textFieldNum.setEnabled(false);
		textFieldNum.setBackground(new Color(230, 230, 230));
		textFieldNum.setBorder(null);
		textFieldNum.setText("0");
		textFieldNum.setFont(new Font("Tahoma", Font.PLAIN, 45));
		textFieldNum.setHorizontalAlignment(JTextField.RIGHT);
		contentPane.add(textFieldNum);
		
		JPanel panelBtns = new JPanel();
		panelBtns.setBounds(10, 215, 500, 335);
		contentPane.add(panelBtns);
		panelBtns.setLayout(new GridLayout(7, 5, 5, 5));
		
		final JLabel labelOperacionCompleta = new JLabel("");
		labelOperacionCompleta.setBounds(10, 11, 500, 14);
		contentPane.add(labelOperacionCompleta);
		
		JLabel labelHistorial = new JLabel("HISTORIAL");
    	labelHistorial.setBounds(625, 11, 100, 25);
    	labelHistorial.setFont(new Font("Tahoma", Font.PLAIN, 20));
    	contentPane.add(labelHistorial);
    	
    	final JPanel panelHistorial = new JPanel();
    	panelHistorial.setBounds(574, 47, 200, 500);
    	panelHistorial.setBackground(new Color(230, 230, 230));
    	contentPane.add(panelHistorial);
    	panelHistorial.setLayout(new GridLayout(15, 1, 2, 0));
    	
    	JButton eliminarHistorial = new JButton("X");
    	eliminarHistorial.setBounds(520, 527, 42, 23);
    	contentPane.add(eliminarHistorial);
	
		for (int i = 0; i < btns.length; i++) {
			btns[i] = new JButton();
			btns[i].setBorder(null);
			panelBtns.add(btns[i]);
		}
		

		btns[0].setText("2nd");
		btns[1].setText("π");
		btns[2].setText("e");
		btns[3].setText("C");
		btns[4].setText("←");
		btns[5].setText("x2");
		btns[6].setText("1/x");
		btns[7].setText("|x|");
		btns[8].setText("exp");
		btns[9].setText("mod");
		btns[10].setText("2√x");
		btns[11].setText("(");
		btns[12].setText(")");
		btns[13].setText("n!");
		btns[14].setText("/");
		btns[15].setText("xy");
		btns[16].setText("7");
		btns[17].setText("8");
		btns[18].setText("9");
		btns[19].setText("x");
		btns[20].setText("10x");
		btns[21].setText("4");
		btns[22].setText("5");
		btns[23].setText("6");
		btns[24].setText("-");
		btns[25].setText("log");
		btns[26].setText("1");
		btns[27].setText("2");
		btns[28].setText("3");
		btns[29].setText("+");
		btns[30].setText("ln");
		btns[31].setText("+/-");
		btns[32].setText("0");
		btns[33].setText(".");
		btns[34].setText("=");



		ActionListener alBtns = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton tmpBtn = (JButton) e.getSource();
				String btnText = tmpBtn.getText();
				
				switch (btnText) {
					case "π":
						if (escribiendoPrimerOperador) {
							num1 = Double.toString(Math.PI);
							textFieldNum.setText(num1);
						}else {
							num2 = Double.toString(Math.PI);
							textFieldNum.setText(num2);
						}
						btns[33].setEnabled(true);							
						break;
						
					case "e":
						if (escribiendoPrimerOperador) {
							num1 = Double.toString(Math.E);
							textFieldNum.setText(num1);
						}else {
							num2 = Double.toString(Math.E);
							textFieldNum.setText(num2);
						}
						btns[33].setEnabled(true);			
						break;
										
					case "C":
						textFieldNum.setText("0");
	
						if (escribiendoPrimerOperador) {
							num1 = textFieldNum.getText();
						}else {
							num2 = textFieldNum.getText();
						}
						btns[33].setEnabled(true);
						break;
						
					case "←":
						int lastIndex = textFieldNum.getText().length()-1;
						
						if (Character.toString(textFieldNum.getText().charAt(lastIndex)).equals(".")) {
							btns[33].setEnabled(true);
						}
						
						char num[] = new char[(textFieldNum.getText().length()-1)];
						
						for (int i = 0; i < (textFieldNum.getText().length()-1); i++) {
							num[i] = textFieldNum.getText().charAt(i);
						}
													
						textFieldNum.setText(new String(num));

						if (textFieldNum.getText().equals("")) {
							textFieldNum.setText("0");
						}
						break;					
						
					case "|x|":
						
						Double resu = 0.0;
						resu = Math.abs(Double.parseDouble(textFieldNum.getText()));
						textFieldNum.setText(Double.toString(resu));
						
						num1= Double.toString(resu);
						btns[34].doClick();
						break;
						
					case "n!":
						
				        double resultado = 1;
				        double cont = Double.parseDouble(textFieldNum.getText());
					
				        for(int i = 1; i <= Double.parseDouble(textFieldNum.getText()); i++) {
							resultado = resultado * cont;		
							cont--;
						}
				        num1 = Double.toString(resultado);
						btns[34].doClick();
						break;
					
					case "(":
					case ")":
						textFieldNum.setText(btnText);
						break;	
						
					case "1/x":
						num2 = num1;
						num1 = "1";
						operacion = "/";			
						btns[34].doClick();
						break;			
					
					case "x2":
						num2 = num1;
						operacion = "x2";			
						btns[34].doClick();
						break;
						
					case "x3":
						num2 = num1;
						operacion = "x3";			
						btns[34].doClick();
						break;
						
					case "2√x":
						num2 = num1;
						operacion = "2√x";			
						btns[34].doClick();
						break;	

					case "3√x":
						num2 = num1;
						operacion = "3√x";			
						btns[34].doClick();
						break;
						
					case "2x":
						num2 = num1;
						operacion = "2x";			
						btns[34].doClick();
						break;	
						
					case "10x":
						num2 = num1;
						operacion = "10x";			
						btns[34].doClick();
						break;		
						
					case "log":
						num2 = num1;
						operacion = "log";			
						btns[34].doClick();
						break;		
					case "ln":
						num2 = num1;
						operacion = "ln";			
						btns[34].doClick();
						break;
						
					case "ex":
						num2 = num1;
						operacion = "ex";			
						btns[34].doClick();
						break;
						
					case "/":	
					case "x":	
					case "-":
					case "+":
					case "%":	
					case "xy":	
					case "x√y":	
					case "exp":
					case "mod":
					case "logyx":
						if (!num1.equals("")) {
							operacion = btnText;
							escribiendoPrimerOperador = false;
							textFieldNum.setText("0");
							btns[33].setEnabled(true);
						}
						break;
						
					case "=":
						if (num1.equals("")) {
							num1 = "0";
						}
						
						switch (operacion) {
							case "/":
								resultado = Double.parseDouble(num1) / Double.parseDouble(num2);
								break;
							case "1/x":
								num2 = num1;
								resultado = 1 / Double.parseDouble(num2);
								break;
							case "x":
								resultado = Double.parseDouble(num1) * Double.parseDouble(num2);			
								break;
							case "x2":
								resultado = Double.parseDouble(num1) * Double.parseDouble(num1);			
								break;
							case "x3":
								resultado = Math.pow(Double.parseDouble(num1), 3);
								break;
							case "-":
								resultado = Double.parseDouble(num1) - Double.parseDouble(num2);
								break;
							case "+":
								resultado = Double.parseDouble(num1) + Double.parseDouble(num2);	
								break;
							case "%":
								resultado = Double.parseDouble(num1) % Double.parseDouble(num2);						
								break;
							case "2√x":				
								resultado = Math.sqrt(Double.parseDouble(num1));
								num2 = ("" + Double.parseDouble(num1));
								num1 = "";
								break;
							case "3√x":
								resultado = Math.cbrt(Double.parseDouble(num1));
								num2 = ("" + Double.parseDouble(num1));
								num1 = "";
								break;
							case "xy":
								resultado = (Math.pow(Double.parseDouble(num1), Double.parseDouble(num2)));
								break;
							case "x√y":
								resultado = Math.pow(Double.parseDouble(num1), 1/Double.parseDouble(num2));
								break;
							case "2x":
								resultado = Math.pow(2, Double.parseDouble(num1));
								break;	
							case "10x":
								resultado = Math.pow(10, Double.parseDouble(num1));
								break;
							case "log":
								resultado = Math.log10(Double.parseDouble(num1)); // que pasa aquiii
								break;
							case "logyx":		
								resultado = Math.log(Double.parseDouble(num1)) / Math.log(Double.parseDouble(num2));
								break;
							case "ln":
								resultado = Math.log(Double.parseDouble(num1));
								break;
							case "ex":
								resultado = Math.pow(Math.E, Double.parseDouble(num1));
								break;
							case "exp":
								resultado = Double.parseDouble(num1) * (Math.pow(10, Double.parseDouble(num2)));
								break;
							case "mod":
								resultado = Double.parseDouble(num1) % Double.parseDouble(num2);
								break;	
							default:
								resultado = Double.parseDouble(num1);
								break;
						}
	
						if (Double.toString(resultado).length() > 4 && resultado != Double.POSITIVE_INFINITY) {
							resultado = Math.round(resultado* 100.0) / 100.0;
						}
						
						if(resultado == Double.POSITIVE_INFINITY){
							textFieldNum.setFont(new Font("Tahoma", Font.PLAIN, 35));
							textFieldNum.setText("No se puede dividir entre cero");
						}else {
							JLabel tmpLabel = new JLabel(num1 + operacion + num2 + " = " + resultado, JLabel.CENTER);
							tmpLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
							panelHistorial.add(tmpLabel);
							
							textFieldNum.setText(Double.toString(resultado));
						}
						
						num2 = "";
						escribiendoPrimerOperador = true;
						operacion = "";
						
						if (textFieldNum.getText().contains(".")) {
							btns[33].setEnabled(false);
						}						
						break;
					case "+/-":
						if (!negativo) {
							negativo = true;
							textFieldNum.setText("-" + textFieldNum.getText());
						}else{
							negativo = false;
							char num2[] = new char[(textFieldNum.getText().length()-1)];
							
							for (int i = 0; i < (textFieldNum.getText().length()-1); i++) {
								num2[i] = textFieldNum.getText().charAt(i+1);
							}
														
							textFieldNum.setText(new String(num2));
						}
						break;
						
					case "2nd":
							
						if (second == false) {
							btns[5].setText("x3");
							btns[10].setText("3√x");
							btns[15].setText("x√y");
							btns[20].setText("2x");
							btns[25].setText("logyx");
							btns[30].setText("ex");
							second = true;
						}
						else {
							btns[5].setText("x2");
							btns[10].setText("2√x");
							btns[15].setText("xy");
							btns[20].setText("10x");
							btns[25].setText("log");
							btns[30].setText("ln");
							second = false;
						}
							
						break;
						
					default:
						textFieldNum.setFont(new Font("Tahoma", Font.PLAIN, 50));
	
						if (textFieldNum.getText().equals("0") || textFieldNum.getText().equals("No se puede dividir entre cero")){
							textFieldNum.setText(btnText);
						}else {
							textFieldNum.setText(textFieldNum.getText() + btnText);
						}

						if (btnText.equals(".")) {
							tmpBtn.setEnabled(false);
						}
						
						break;
				}

				if (escribiendoPrimerOperador) {
					num1 = textFieldNum.getText();
				}else {
					num2 = textFieldNum.getText();
				}
				
				if(!num1.equals("No se puede dividir entre cero")) {
					labelOperacionCompleta.setText(num1 + operacion + num2);
				}else {
					labelOperacionCompleta.setText("No se puede dividir entre cero");
				}
				
			}
		};
		
		for (int i = 0; i < btns.length; i++) {		
			if (btns[i].getText().equals(".") || btns[i].getText().equals("0") || btns[i].getText().equals("+/-") || btns[i].getText().equals("3") || btns[i].getText().equals("2") || btns[i].getText().equals("1") || btns[i].getText().equals("6") || btns[i].getText().equals("5") || btns[i].getText().equals("4") || btns[i].getText().equals("9") || btns[i].getText().equals("8") || btns[i].getText().equals("7")) {
				btns[i].setBackground( new Color(250, 250, 250));
			}else if(btns[i].getText().equals("=")) {
				btns[i].setBackground( new Color(154, 186, 219));
			}else {
				btns[i].setBackground( new Color(240, 240, 240));
			}
			
			btns[i].addActionListener(alBtns);
		}
		
		ActionListener alEliminarHistorial = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panelHistorial.removeAll();
				panelHistorial.repaint();
			}
		};
		
		eliminarHistorial.addActionListener(alEliminarHistorial);

	}
}