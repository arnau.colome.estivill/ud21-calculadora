package ejercicios.ejercicios;

import static org.junit.jupiter.api.Assertions.*;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import views.AplicacionGrafica;


public class AppTest 
{
	
	public static Stream<Arguments> Spi(){
		return Stream.of(
			Arguments.of(2, 12.5664),
			Arguments.of(4, 50.2656),
			Arguments.of(11, 380.1336));
	}
	
	@ParameterizedTest
	@MethodSource("Spi")
	public void testpi (int a, double b) {
		AplicacionGrafica c = new AplicacionGrafica();
		double result = c.(btnText("π"))(a);
		assertEquals(b, result);
	}

	
	
}
