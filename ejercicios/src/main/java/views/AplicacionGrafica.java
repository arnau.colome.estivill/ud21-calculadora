package views;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class AplicacionGrafica extends JFrame {

	//variables
	private JPanel contentPane;
	private JPanel paneles = new JPanel();
	private JLabel etiqueta[] = new JLabel[3];
	private JButton boton[] = new JButton[24];
	private JTextField numero[] = new JTextField[1];
	private double num1=0;
	private double num2=0;
	private double resultado;
	private boolean dividir1=false, multiplicar1=false, resta1=false, suma1=false;

	public AplicacionGrafica() {
		setTitle("Calculadora");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 630);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Historial
		etiqueta[0] = new JLabel("");
		etiqueta[0].setBounds(433, 64, 202, 446);
		contentPane.add(etiqueta[0]);
		etiqueta[0].setLayout(new GridLayout(0, 4, 0, 0));
		
		etiqueta[1] = new JLabel("CALCULADORA: ");
		etiqueta[1].setBounds(20, 65, 118, 27);
		contentPane.add(etiqueta[1]);
		
		etiqueta[1] = new JLabel();
		etiqueta[1].setBounds(200, 65, 150, 27);
		contentPane.add(etiqueta[1]);
		
		numero[0] = new JTextField();
		numero[0].setBounds(153, 99, 222, 55);
		numero[0].setEnabled(false);
		numero[0].setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(numero[0]);
		
		etiqueta[2] = new JLabel("Historial");
		etiqueta[2].setBounds(433, 36, 55, 23);
		contentPane.add(etiqueta[2]);

		paneles = new JPanel();
		paneles.setBounds(23, 169, 352, 341);
		contentPane.add(paneles);
		paneles.setLayout(new GridLayout(6, 4, 0, 0));		
		
		for (int i = 0; i <= 23; i++) {
			boton[i] = new JButton();
			paneles.add(boton[i]);
		}
		
		boton[0].setText("%");
		boton[1].setText("CE");
		boton[2].setText("C");
		boton[3].setText("BORRAR");
		boton[4].setText("1/x");
		boton[5].setText("x2");
		boton[6].setText("2√x");
		boton[7].setText("·/.");
		boton[8].setText("7");
		boton[9].setText("8");
		boton[10].setText("9");
		boton[11].setText("x");
		boton[12].setText("4");
		boton[13].setText("5");
		boton[14].setText("6");
		boton[15].setText("-");
		boton[16].setText("1");
		boton[17].setText("2");
		boton[18].setText("3");
		boton[19].setText("+");
		boton[20].setText("+/-");
		boton[21].setText("0");
		boton[22].setText(",");
		boton[23].setText("=");
		
		
		//listeners
		
		ActionListener perc = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				numero[0].setText("0.0"+ numero[0].getText());
				
			}
		};
		boton[0].addActionListener(perc);

		//Borrrar historico
		ActionListener CE = new ActionListener() { 
			public void actionPerformed (ActionEvent e) {
		
				etiqueta[0].setText("");
				
			}
		};
		boton[1].addActionListener(CE);
		
		//borrar campo
		ActionListener C = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
		
				etiqueta[1].setText("");
				numero[0].setText("");
				
				
			}
		};
		boton[2].addActionListener(C);
		
		//borrar ultima cifra metida
		ActionListener BORRAR = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				if (numero[0].getText().equals("")) {
					numero[0].setText("0");
				}
				else {
					numero[0].setText(numero[0].getText().substring(1));
				}	
			}
		};
		boton[3].addActionListener(BORRAR);
		
		//dividir ese numero
		ActionListener dividir = new ActionListener() {
			public void actionPerformed (ActionEvent e) {	
				double op = Double.parseDouble(numero[0].getText());
				double a = 1 / op;
				String b = Double.toString(a);
				
				if (numero[0].getText().equals("")) {
					numero[0].setText("No se puede dividir por 0");
				}
				else {
					numero[0].setText(b);
				}	
			}
		};
		boton[4].addActionListener(dividir);
		
		//cuadrado
		ActionListener cuadrado = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				double op = Double.parseDouble(numero[0].getText());
				double a = Math.pow(op, 2);
				String b = Double.toString(a);
				
				if (numero[0].getText().equals("")) {
					numero[0].setText("0");
				}
				else {
					numero[0].setText(b);
				}
			}
		};
		boton[5].addActionListener(cuadrado);
		
		//raiz
		ActionListener raiz = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				double op = Double.parseDouble(numero[0].getText());
				double a = Math.sqrt(op);
				String b = Double.toString(a);
				if (numero[0].getText().equals("")) {
					numero[0].setText("0");
				}
				else {
					numero[0].setText(b);
				}
			}
		};
		boton[6].addActionListener(raiz);		
		
		//dividir
		ActionListener div = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

				if (numero[0].getText().equals("")) {
					numero[0].setText("0");
				}
				else {
					num1 = Double.parseDouble(numero[0].getText());
					etiqueta[1].setText(num1 + " / " );
					numero[0].setText("");
					dividir1= true;
				}
			}
		};
		boton[7].addActionListener(div);
		
		
		ActionListener siete = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + "7");

			}
		};
		boton[8].addActionListener(siete);
		
		
		ActionListener ocho = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + "8");

			}
		};
		boton[9].addActionListener(ocho);	
		
		
		ActionListener nueve = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + "9");

			}
		};
		boton[10].addActionListener(nueve);
		
		
		ActionListener multi = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

				if (numero[0].getText().equals("")) {
					numero[0].setText("0");
				}
				else {
					num1 = Double.parseDouble(numero[0].getText());
					etiqueta[1].setText(num1 + " * " );
					numero[0].setText("");
					multiplicar1= true;
				}
			}
		};
		boton[11].addActionListener(multi);
		
		
		ActionListener cuatro = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + "4");

			}
		};
		boton[12].addActionListener(cuatro);
		
		
		ActionListener cinco = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + "5");

			}
		};
		boton[13].addActionListener(cinco);	
		
		
		ActionListener seis = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + "6");

			}
		};
		boton[14].addActionListener(seis);
		
		
		ActionListener resta = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

				if (numero[0].getText().equals("")) {
					numero[0].setText("0");
				}
				else {
					num1 = Double.parseDouble(numero[0].getText());
					etiqueta[1].setText(num1 + " - " );
					numero[0].setText("");
					resta1= true;
				}

			}
		};
		boton[15].addActionListener(resta);
		
		
		ActionListener uno = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + "1");

			}
		};
		boton[16].addActionListener(uno);
		
		
		ActionListener dos = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + "2");

			}
		};
		boton[17].addActionListener(dos);	
		
		
		ActionListener tres = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + "3");

			}
		};
		boton[18].addActionListener(tres);
		
		
		ActionListener suma = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

				if (numero[0].getText().equals("")) {
					numero[0].setText("0");
				}
				else {
					num1 = Double.parseDouble(numero[0].getText());
					etiqueta[1].setText(num1 + " + " );
					numero[0].setText("");
					suma1= true;
				}

			}
		};
		boton[19].addActionListener(suma);
		
		
		ActionListener res = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
			
				numero[0].setText(" - " + numero[0].getText());
				
			}
		};
		boton[20].addActionListener(res);
		
		
		ActionListener cero = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + "0");

			}
		};
		boton[21].addActionListener(cero);
		
		
		ActionListener punto = new ActionListener() {
			public void actionPerformed (ActionEvent e) {

					numero[0].setText(numero[0].getText() + ".");

			}
		};
		boton[22].addActionListener(punto);
		
		
		ActionListener resu = new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				num2 = Double.parseDouble(numero[0].getText());
				
					if (dividir1 == true) {	
						resultado = num1 / num2;
						etiqueta[1].setText(num1 + " / " + num2 + " = " + resultado);
						etiqueta[0].setText(etiqueta[0].getText() + num1 + " / " + num2 + " = " + resultado + "     ");
					}
					else if (multiplicar1 == true) {
						resultado = num1 * num2;
						etiqueta[1].setText(num1 + " * " + num2 + " = " + resultado);
						etiqueta[0].setText(etiqueta[0].getText() + num1 + " * " + num2 + " = " + resultado + "     ");
					}
					else if (resta1 == true) {
						resultado = num1 - num2;
						etiqueta[1].setText(num1 + " - " + num2 + " = " + resultado);
						etiqueta[0].setText(etiqueta[0].getText() + num1 + " - " + num2 + " = " + resultado + "     ");
					}
					else if(suma1 == true) {
						resultado = num1 + num2;
						etiqueta[1].setText(num1 + " + " + num2 + " = " + resultado);
						etiqueta[0].setText(etiqueta[0].getText() + num1 + " + " + num2 + " = " + resultado);
					}
					numero[0].setText("" + resultado);
					

			}
		};
		boton[23].addActionListener(resu);
		
	}
}
