package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class AplicacionGrafica extends JFrame{
	//Definimos y inicializamos las variables
	private JPanel contentPane;
	private String moneda1 = "Argentina - Peso";
	private String moneda2 = "Argentina - Peso";
	private String num1 = "";
	private String num2 = "";
	private String operacion = "";
	private boolean escribiendoPrimerOperador = true;
	private JButton btns[] = new JButton[13];
	private JButton btnsMoneda[] = new JButton[118];
	private JTextField textField;
	private JTextField textField1;
	private JTextField textFieldNum;
	private JTextField textFieldNum1;
	private double proporcion = 1.0;
	DecimalFormat decimal = new DecimalFormat("0.00");
	private String result = null;
	private double resultado;
	private double num1Double;
	

	public AplicacionGrafica() {
		//Creamos los elementos de la aplicación gráfica
		setIconImage(Toolkit.getDefaultToolkit().getImage(AplicacionGrafica.class.getResource("/views/calculadora.PNG")));
		setTitle("Calculadora Moneda");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);

		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(new Color(230, 230, 230));
		
		//Texto dónde introducimos la cantidad de dinero que tenemos
		final JTextField textFieldNum = new JTextField();
		textFieldNum.setBounds(43, 63, 233, 93);
		textFieldNum.setText("");
		textFieldNum.setEnabled(false);
		textFieldNum.setBackground(new Color(230, 230, 230));
		textFieldNum.setBorder(null);
		textFieldNum.setText("0");
		textFieldNum.setFont(new Font("Tahoma", Font.PLAIN, 45));
		textFieldNum.setHorizontalAlignment(SwingConstants.LEFT);
		contentPane.add(textFieldNum);
		
		JPanel panelBtns = new JPanel();
		panelBtns.setBounds(521, 87, 249, 419);
		contentPane.add(panelBtns);
		panelBtns.setLayout(new GridLayout(5, 5, 5, 5));
		
		
		JPanel panelBtns_1 = new JPanel();
		panelBtns_1.setBounds(389, 174, 128, 244);
		contentPane.add(panelBtns_1);
		panelBtns_1.setLayout(new GridLayout(3, 5, 5, 5));
		
		//Texto que nos retrona la cantidad de dinero en otra moneda
		final JTextField textFieldNum1 = new JTextField();
		textFieldNum1.setBounds(43, 273, 233, 93);
		textFieldNum1.setText("");
		textFieldNum1.setEnabled(false);
		textFieldNum1.setBackground(new Color(230, 230, 230));
		textFieldNum1.setBorder(null);
		textFieldNum1.setText("0");
		textFieldNum1.setFont(new Font("Tahoma", Font.PLAIN, 45));
		textFieldNum1.setHorizontalAlignment(SwingConstants.LEFT);
		contentPane.add(textFieldNum1);
		
		JComboBox comboBox = new JComboBox();
		
		//ComboBox que nos sirve para elegir que moneda estamos utilizando
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Argentina - Peso", "Brasil - Real","Dinamarca - Corona", "Estados Unidos - Dólar", "Etiopía - Birr", "Europa - Euro", "Indonesia - Rupia","Moldavia - Leu","Reino Unido - Libras", "República Dominicana - Peso"}));
		comboBox.setBounds(43, 170, 173, 25);
		contentPane.add(comboBox);
		
		//ComboBox que nos sirve para elegir a qué moneda queremos convertir el dinero
		JComboBox comboBox_1 = new JComboBox();
		
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Argentina - Peso", "Brasil - Real","Dinamarca - Corona", "Estados Unidos - Dólar", "Etiopía - Birr", "Europa - Euro", "Indonesia - Rupia","Moldavia - Leu","Reino Unido - Libras", "República Dominicana - Peso"}));
		comboBox_1.setBounds(43, 369, 173, 25);
		contentPane.add(comboBox_1);
		
		//texto dónde introducimos la conversión
		final JTextField textField2 = new JTextField();
		textField2.setBounds(43, 481, 216, 25);
		textField2.setBorder(null);
		textField2.setEnabled(false);
		textField2.setText(" 1 $ = 1.0 $");
		textField2.setBackground(new Color(230, 230, 230));
		textField2.setFont(new Font("Tahoma", Font.PLAIN, 25));
		contentPane.add(textField2);
		
		//texto dónde introducimos el símbolo del dinero convertido
		final JTextField textField = new JTextField();
		textField.setBounds(288, 286, 87, 68);
		textField.setText("$");
		textField.setEnabled(false);
		textField.setBackground(new Color(230, 230, 230));
		textField.setBorder(null);
		
		textField.setFont(new Font("Tahoma", Font.PLAIN, 45));
		textField.setHorizontalAlignment(SwingConstants.LEFT);
		contentPane.add(textField);
		
		//Texto dónde introducimos el símbolo del dinero que tenemos
		final JTextField textField1 = new JTextField();
		textField1.setBounds(288, 77, 87, 66);
		textField1.setText("$");
		textField1.setEnabled(false);
		textField1.setBackground(new Color(230, 230, 230));
		textField1.setBorder(null);
		
		textField1.setFont(new Font("Tahoma", Font.PLAIN, 45));
		textField1.setHorizontalAlignment(SwingConstants.LEFT);
		contentPane.add(textField1);
		
		//Creamos los botones de la calculadora
		for (int i = 0; i < btnsMoneda.length; i++) {
			btnsMoneda[i] = new JButton();
			btnsMoneda[i].setText("num2");
			comboBox.add(btnsMoneda[i] );
		}
	
		//Creamos los botones de la calculadora
		for (int i = 0; i < btns.length; i++) {
			btns[i] = new JButton();
			btns[i].setBorder(null);
		
			if (i == 2 || i == 5 || i == 8) {  
				panelBtns_1.add(btns[i]);
			}else {
				panelBtns.add(btns[i]);
			}
		}
		

		btns[0].setText("CE");
		btns[1].setText("←");
		btns[2].setText("7");
		btns[3].setText("8");
		btns[4].setText("9");
		btns[5].setText("4");
		btns[6].setText("5");
		btns[7].setText("6");
		btns[8].setText("1");
		btns[9].setText("2");
		btns[10].setText("3");
		btns[11].setText("0");
		btns[12].setText(".");

		//Introducimos el que tiene que hacer la aplicación al dar clic a cada botón
		ActionListener alBtns = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton tmpBtn = (JButton) e.getSource();
				String btnText = tmpBtn.getText();
				
				switch (btnText) {
										
					case "CE":
						textFieldNum.setText("0");
						textFieldNum1.setText("0");
						btns[12].setEnabled(true);
						break;
						
					case "←":
						int lastIndex = textFieldNum.getText().length()-1;
						
						if (Character.toString(textFieldNum.getText().charAt(lastIndex)).equals(".")) {
							btns[12].setEnabled(true);
						}
						
						char num[] = new char[(textFieldNum.getText().length()-1)];
						
						for (int i = 0; i < (textFieldNum.getText().length()-1); i++) {
							num[i] = textFieldNum.getText().charAt(i);
						}
													
						textFieldNum.setText(new String(num));

						if (textFieldNum.getText().equals("")) {
							textFieldNum.setText("0");
						}
						break;					
											
					default:
						textFieldNum.setFont(new Font("Tahoma", Font.PLAIN, 50));
	
						if (textFieldNum.getText().equals("0")){
							textFieldNum.setText(btnText);
						}else {
							textFieldNum.setText(textFieldNum.getText() + btnText);
						}

						if (btnText.equals(".")) {
							tmpBtn.setEnabled(false);
						}
						
						break;
				}				
				
				num1 = textFieldNum.getText();
				num2 = textFieldNum1.getText();
				num1Double = Double.parseDouble(num1);
				
				//Hacemos la conversión
				moneda1 = (String) comboBox.getSelectedItem();
			    moneda2 = (String) comboBox_1.getSelectedItem();
			    primeraConversio();
				segonaConversio();
				
				resultado = num1Double * proporcion;
				result = decimal.format(resultado);
				textFieldNum1.setText(result);
				textField2.setText("1 " + textField1.getText() + " = " + proporcion + " " + textField.getText());
				
				if (moneda1.equals(moneda2)) {
					textFieldNum1.setText(textFieldNum.getText());
				}
				
				comboBox.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						moneda1 = (String) comboBox.getSelectedItem();
					    moneda2 = (String) comboBox_1.getSelectedItem();
					    //Definimos el símbolo de la moneda del dinero que tenemos
					    switch (moneda1) {
							case "Argentina - Peso":
								textField1.setText("$");
								break;
							case "Brasil - Real":
								textField1.setText("R$");
								break;
							case "Dinamarca - Corona":
								textField1.setText("kr");
								break;
							case "Estados Unidos - Dólar":	
								textField1.setText("$");
								break;
							case "Etiopía - Birr":
								textField1.setText("ETB");
								break;
							case  "Europa - Euro":
								textField1.setText("€");
								break;
							case "Indonesia - Rupia":
								textField1.setText("Rp");
								break;
							case "Moldavia - Leu":
								textField1.setText("MDL");
								break;
							case "Reino Unido - Libras":
								textField1.setText("£");
								break;
							case "República Dominicana - Peso":
								textField1.setText("RD$");
								break;
					    }
						primeraConversio();	
					    resultado = num1Double * proporcion;
						result = decimal.format(resultado);
						textFieldNum1.setText(result);
						textField2.setText("1 " + textField1.getText() + " = " + proporcion + " " + textField.getText());
					}
				});
				
				comboBox_1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						moneda1 = (String) comboBox.getSelectedItem();
					    moneda2 = (String) comboBox_1.getSelectedItem();
					    switch (moneda2) {
					    //Definimos el símbolo de la moneda a la que convertimos el dinero
						case "Argentina - Peso":
							textField.setText("$");	
						break;
						case "Brasil - Real":
							textField.setText("R$");
							break;
						case "Dinamarca - Corona":
							textField.setText("kr");
							break;
						case "Estados Unidos - Dólar":	
							textField.setText("$");
							break;
						case "Etiopía - Birr":
							textField.setText("ETB");
							break;
						case  "Europa - Euro":
							textField.setText("€");
							break;
						case "Indonesia - Rupia":
							textField.setText("Rp");
							break;
						case "Moldavia - Leu":
							textField.setText("MDL");
							break;
						case "Reino Unido - Libras":
							textField.setText("£");
							break;
						case "República Dominicana - Peso":
							textField.setText("RD$");
							break;
				    }
					    segonaConversio();
					    resultado = num1Double * proporcion;
						result = decimal.format(resultado);
						textFieldNum1.setText(result);
						textField2.setText("1 " + textField1.getText() + " = " + proporcion + " " + textField.getText());
					}
				});
			}
		};
		//Hacemos que la aplicación cambie el color del botón al dar click
		for (int i = 0; i < btns.length; i++) {		
			if (btns[i].getText().equals(".") || btns[i].getText().equals("0") || btns[i].getText().equals("3") || btns[i].getText().equals("2") || btns[i].getText().equals("1") || btns[i].getText().equals("6") || btns[i].getText().equals("5") || btns[i].getText().equals("4") || btns[i].getText().equals("9") || btns[i].getText().equals("8") || btns[i].getText().equals("7")) {
				btns[i].setBackground( new Color(250, 250, 250));
			}else {
				btns[i].setBackground( new Color(240, 240, 240));
			}
			
			btns[i].addActionListener(alBtns);
		}
		
	}
	//Definimos la proporciones de cambio de moneda
	public void primeraConversio () {
		 switch (moneda1) {
			case "Argentina - Peso":
				if (moneda1.equals("Argentina - Peso")) 
					proporcion = 1.0;
				if (moneda2.equals("Brasil - Real")) 
					proporcion = 0.07597;	
				if (moneda2.equals("Dinamarca - Corona")) 
					proporcion = 0.08541;
				if (moneda2.equals("Estados Unidos - Dólar")) 
					proporcion = 0.01353;
				if (moneda2.equals("Etiopía - Birr")) 
					proporcion = 0.4877;
				if (moneda2.equals("Europa - Euro")) 
					proporcion = 0.01147;
				if (moneda2.equals("Indonesia - Rupia")) 
					proporcion = 198.1604;
				if (moneda2.equals("Moldavia - Leu")) 
					proporcion = 0.2238;
				if (moneda2.equals("Reino Unido - Libras")) 
					proporcion = 0.01026;
				if (moneda2.equals("República Dominicana - Peso")) 
					proporcion = 0.7895;
				break;
			case "Brasil - Real":
				if (moneda1.equals("Brasil - Real")) 
					proporcion = 1.0;
				if (moneda2.equals("Argentina - Peso")) 
					proporcion = 13.1632;	
				if (moneda2.equals("Dinamarca - Corona")) 
					proporcion = 1.1242;
				if (moneda2.equals("Estados Unidos - Dólar")) 
					proporcion = 0.178;
				if (moneda2.equals("Etiopía - Birr")) 
					proporcion = 6.4192;
				if (moneda2.equals("Europa - Euro")) 
					proporcion = 0.151;
				if (moneda2.equals("Indonesia - Rupia")) 
					proporcion = 2608.2467;
				if (moneda2.equals("Moldavia - Leu")) 
					proporcion = 2.9458;
				if (moneda2.equals("Reino Unido - Libras")) 
					proporcion = 0.1351;
				if (moneda2.equals("República Dominicana - Peso")) 
					proporcion = 10.392;
				break;
			case "Dinamarca - Corona":
				if (moneda1.equals("Dinamarca - Corona")) 
					proporcion = 1.0;
				if (moneda2.equals("Argentina - Peso")) 
					proporcion = 11.7082;	
				if (moneda2.equals("Brasil - Real")) 
					proporcion = 0.8895;
				if (moneda2.equals("Estados Unidos - Dólar")) 
					proporcion = 0.1584;
				if (moneda2.equals("Etiopía - Birr")) 
					proporcion = 5.71;
				if (moneda2.equals("Europa - Euro")) 
					proporcion = 0.1343;
				if (moneda2.equals("Indonesia - Rupia")) 
					proporcion = 2320.0938;
				if (moneda2.equals("Moldavia - Leu")) 
					proporcion = 2.6204;
				if (moneda2.equals("Reino Unido - Libras")) 
					proporcion = 0.1202;
				if (moneda2.equals("República Dominicana - Peso")) 
					proporcion = 9.244;
				break;
			case "Estados Unidos - Dólar":	
				if (moneda1.equals("Estados Unidos - Dólar")) 
					proporcion = 1.0;
				if (moneda2.equals("Argentina - Peso")) 
					proporcion = 73.93;	
				if (moneda2.equals("Dinamarca - Corona")) 
					proporcion = 6.3144;
				if (moneda2.equals("Brasil - Real")) 
					proporcion = 5.6168;
				if (moneda2.equals("Etiopía - Birr")) 
					proporcion = 36.0553;
				if (moneda2.equals("Europa - Euro")) 
					proporcion = 0.8482;
				if (moneda2.equals("Indonesia - Rupia")) 
					proporcion = 14650.00;
				if (moneda2.equals("Moldavia - Leu")) 
					proporcion = 16.546;
				if (moneda2.equals("Reino Unido - Libras")) 
					proporcion = 0.7587;
				if (moneda2.equals("República Dominicana - Peso")) 
					proporcion = 58.37;
				break;
			case "Etiopía - Birr":
				if (moneda1.equals("Etiopía - Birr")) 
					proporcion = 1.0;
				if (moneda2.equals("Argentina - Peso")) 
					proporcion = 2.0505;	
				if (moneda2.equals("Dinamarca - Corona")) 
					proporcion = 0.1751;
				if (moneda2.equals("Estados Unidos - Dólar")) 
					proporcion = 0.02774;
				if (moneda2.equals("Brasil - Real")) 
					proporcion = 0.1558;
				if (moneda2.equals("Europa - Euro")) 
					proporcion = 0.02352;
				if (moneda2.equals("Indonesia - Rupia")) 
					proporcion = 406.3203;
				if (moneda2.equals("Moldavia - Leu")) 
					proporcion = 0.4589;
				if (moneda2.equals("Reino Unido - Libras")) 
					proporcion = 0.02104;
				if (moneda2.equals("República Dominicana - Peso")) 
					proporcion = 1.6189;
				break;
			case  "Europa - Euro":
				if (moneda1.equals("Europa - Euro")) 
					proporcion = 1.0;
				if (moneda2.equals("Argentina - Peso")) 
					proporcion = 87.161;	
				if (moneda2.equals("Dinamarca - Corona")) 
					proporcion = 7.4445;
				if (moneda2.equals("Estados Unidos - Dólar")) 
					proporcion = 1.179;
				if (moneda2.equals("Etiopía - Birr")) 
					proporcion = 42.508;
				if (moneda2.equals("Brasil - Real")) 
					proporcion = 6.622;
				if (moneda2.equals("Indonesia - Rupia")) 
					proporcion = 17271.8698;
				if (moneda2.equals("Moldavia - Leu")) 
					proporcion = 19.5072;
				if (moneda2.equals("Reino Unido - Libras")) 
					proporcion = 0.8945;
				if (moneda2.equals("República Dominicana - Peso")) 
					proporcion = 68.8163;
				break;
			case "Indonesia - Rupia":
				if (moneda1.equals("Indonesia - Rupia")) 
					proporcion = 1.0;
				if (moneda2.equals("Argentina - Peso")) 
					proporcion = 0.005046;
				if (moneda2.equals("Dinamarca - Corona")) 
					proporcion = 0.000431;
				if (moneda2.equals("Estados Unidos - Dólar")) 
					proporcion = 0.00006826;
				if (moneda2.equals("Etiopía - Birr")) 
					proporcion = 0.002461;
				if (moneda2.equals("Europa - Euro")) 
					proporcion = 0.0000579;
				if (moneda2.equals("Brasil - Real")) 
					proporcion = 0.0003834;
				if (moneda2.equals("Moldavia - Leu")) 
					proporcion = 0.001129;
				if (moneda2.equals("Reino Unido - Libras")) 
					proporcion = 0.00005179;
				if (moneda2.equals("República Dominicana - Peso")) 
					proporcion = 0.003984;
				break;
			case "Moldavia - Leu":
				if (moneda1.equals("Moldavia - Leu")) 
					proporcion = 1.0;
				if (moneda2.equals("Argentina - Peso")) 
					proporcion = 4.4681;	
				if (moneda2.equals("Dinamarca - Corona")) 
					proporcion = 0.3816;
				if (moneda2.equals("Estados Unidos - Dólar")) 
					proporcion = 0.06044;
				if (moneda2.equals("Etiopía - Birr")) 
					proporcion = 2.1791;
				if (moneda2.equals("Europa - Euro")) 
					proporcion = 0.05126;
				if (moneda2.equals("Indonesia - Rupia")) 
					proporcion = 885.4104;
				if (moneda2.equals("Brasil - Real")) 
					proporcion = 0.3395;
				if (moneda2.equals("Reino Unido - Libras")) 
					proporcion = 0.04585;
				if (moneda2.equals("República Dominicana - Peso")) 
					proporcion = 3.5277;
				break;
			case "Reino Unido - Libras":
				if (moneda1.equals("Reino Unido - Libras")) 
					proporcion = 1.0;	
				if (moneda2.equals("Argentina - Peso")) 
					proporcion = 97.443;	
				if (moneda2.equals("Dinamarca - Corona")) 
					proporcion = 8.3227;
				if (moneda2.equals("Estados Unidos - Dólar")) 
					proporcion = 1.318;
				if (moneda2.equals("Etiopía - Birr")) 
					proporcion = 47.522;
				if (moneda2.equals("Europa - Euro")) 
					proporcion = 1.118;
				if (moneda2.equals("Indonesia - Rupia")) 
					proporcion = 19309.3449;
				if (moneda2.equals("Moldavia - Leu")) 
					proporcion = 21.8084;
				if (moneda2.equals("Brasil - Real")) 
					proporcion = 7.4032;
				if (moneda2.equals("República Dominicana - Peso")) 
					proporcion = 76.9342;
				break;
			case "República Dominicana - Peso":
				if (moneda1.equals("República Dominicana - Peso")) 
					proporcion = 1.0;	
				if (moneda2.equals("Argentina - Peso")) 
					proporcion = 1.2666;	
				if (moneda2.equals("Dinamarca - Corona")) 
					proporcion = 0.1082;
				if (moneda2.equals("Estados Unidos - Dólar")) 
					proporcion = 0.01713;
				if (moneda2.equals("Etiopía - Birr")) 
					proporcion = 0.6177;
				if (moneda2.equals("Europa - Euro")) 
					proporcion = 0.01453;
				if (moneda2.equals("Indonesia - Rupia")) 
					proporcion = 250.9851;
				if (moneda2.equals("Moldavia - Leu")) 
					proporcion = 0.2835;
				if (moneda2.equals("Reino Unido - Libras")) 
					proporcion = 0.013;
				if (moneda2.equals("Brasil - Real")) 
					proporcion = 0.09623;
				break;
		 }
	}
	//Definimos las proporciones de cambio de moneda
	public void segonaConversio() {
		switch (moneda2) {
		case "Argentina - Peso":
			if (moneda1.equals("Argentina - Peso")) 
				proporcion = 1.0;
			if (moneda1.equals("República Dominicana - Peso")) 
				proporcion = 1.2666;	
			if (moneda1.equals("Reino Unido - Libras")) 
				proporcion = 97.443;	
			if (moneda1.equals("Moldavia - Leu")) 
				proporcion = 4.4681;
			if (moneda1.equals("Indonesia - Rupia")) 
				proporcion = 0.005046;	
			if (moneda1.equals("Europa - Euro")) 
				proporcion = 87.161;	
			if (moneda1.equals("Etiopía - Birr")) 
				proporcion = 2.0505;
			if (moneda1.equals("Estados Unidos - Dólar")) 
				proporcion = 73.93;	
			if (moneda1.equals("Dinamarca - Corona")) 
				proporcion = 11.7082;	
			if (moneda1.equals("Brasil - Real")) 
				proporcion = 13.1632;	
		break;
		case "Brasil - Real":
			if (moneda1.equals("Brasil - Real")) 
				proporcion = 1.0;		
			if (moneda1.equals("Argentina - Peso")) 
				proporcion = 0.07597;
			if (moneda1.equals("República Dominicana - Peso")) 
				proporcion = 0.09623;
			if (moneda1.equals("Reino Unido - Libras")) 
				proporcion = 7.4032;
			if (moneda1.equals("Moldavia - Leu")) 
				proporcion = 0.3395;
			if (moneda1.equals("Indonesia - Rupia")) 
				proporcion = 0.0003834;
			if (moneda1.equals("Europa - Euro")) 
				proporcion = 6.622;
			if (moneda1.equals("Etiopía - Birr")) 
				proporcion = 0.1558;
			if (moneda1.equals("Estados Unidos - Dólar")) 
				proporcion = 5.6168;
			if (moneda1.equals("Dinamarca - Corona")) 
				proporcion = 0.8895;
			break;
		case "Dinamarca - Corona":
			if (moneda1.equals("Dinamarca - Corona")) 
				proporcion = 1.0;
			if (moneda1.equals("Argentina - Peso")) 
				proporcion = 0.08541;
			if (moneda1.equals("República Dominicana - Peso")) 
				proporcion = 0.1082;
			if (moneda1.equals("Reino Unido - Libras")) 
				proporcion = 8.3227;
			if (moneda1.equals("Moldavia - Leu")) 
				proporcion = 0.3816;
			if (moneda1.equals("Indonesia - Rupia")) 
				proporcion = 0.000431;
			if (moneda1.equals("Europa - Euro")) 
				proporcion = 7.4445;
			if (moneda1.equals("Etiopía - Birr")) 
				proporcion = 0.1751;
			if (moneda1.equals("Estados Unidos - Dólar")) 
				proporcion = 6.3144;
			if (moneda1.equals("Brasil - Real")) 
				proporcion = 1.1242;
			break;
		case "Estados Unidos - Dólar":	
			if (moneda1.equals("Estados Unidos - Dólar")) 
				proporcion = 1.0;
			if (moneda1.equals("Argentina - Peso")) 
				proporcion = 0.01353;
			if (moneda1.equals("República Dominicana - Peso")) 
				proporcion = 0.01713;
			if (moneda1.equals("Reino Unido - Libras")) 
				proporcion = 1.318;
			if (moneda1.equals("Moldavia - Leu")) 
				proporcion = 0.06044;
			if (moneda1.equals("Indonesia - Rupia")) 
				proporcion = 0.00006826;
			if (moneda1.equals("Europa - Euro")) 
				proporcion = 1.179;
			if (moneda1.equals("Etiopía - Birr")) 
				proporcion = 0.02774;
			if (moneda1.equals("Dinamarca - Corona")) 
				proporcion = 0.1584;
			if (moneda1.equals("Brasil - Real")) 
				proporcion = 0.178;
			break;
		case "Etiopía - Birr":
			if (moneda1.equals("Etiopía - Birr")) 
				proporcion = 1.0;
			if (moneda1.equals("Argentina - Peso")) 
				proporcion = 0.4877;
			if (moneda1.equals("República Dominicana - Peso")) 
				proporcion = 0.6177;
			if (moneda1.equals("Reino Unido - Libras")) 
				proporcion = 47.522;
			if (moneda1.equals("Moldavia - Leu")) 
				proporcion = 2.1791;
			if (moneda1.equals("Indonesia - Rupia")) 
				proporcion = 0.002461;
			if (moneda1.equals("Europa - Euro")) 
				proporcion = 42.508;
			if (moneda1.equals("Estados Unidos - Dólar")) 
				proporcion = 36.0553;
			if (moneda1.equals("Dinamarca - Corona")) 
				proporcion = 5.71;
			if (moneda1.equals("Brasil - Real")) 
				proporcion = 6.4192;
			break;
		case  "Europa - Euro":
			if (moneda1.equals("Europa - Euro")) 
				proporcion = 1.0;
			if (moneda1.equals("Argentina - Peso")) 
				proporcion = 0.01147;
			if (moneda1.equals("República Dominicana - Peso")) 
				proporcion = 0.01453;
			if (moneda1.equals("Reino Unido - Libras")) 
				proporcion = 1.118;
			if (moneda1.equals("Moldavia - Leu")) 
				proporcion = 0.05126;
			if (moneda1.equals("Indonesia - Rupia")) 
				proporcion = 0.0000579;
			if (moneda1.equals("Etiopía - Birr")) 
				proporcion = 0.02352;
			if (moneda1.equals("Estados Unidos - Dólar")) 
				proporcion = 0.8482;
			if (moneda1.equals("Dinamarca - Corona")) 
				proporcion = 0.1343;
			if (moneda1.equals("Brasil - Real")) 
				proporcion = 0.151;
			break;
		case "Indonesia - Rupia":
			if (moneda1.equals("Indonesia - Rupia")) 
				proporcion = 1.0;
			if (moneda1.equals("Argentina - Peso")) 
				proporcion = 198.1604;
			if (moneda1.equals("República Dominicana - Peso")) 
				proporcion = 250.9851;
			if (moneda1.equals("Reino Unido - Libras")) 
				proporcion = 19309.3449;
			if (moneda1.equals("Moldavia - Leu")) 
				proporcion = 885.4104;
			if (moneda1.equals("Europa - Euro")) 
				proporcion = 17271.8698;
			if (moneda1.equals("Etiopía - Birr")) 
				proporcion = 406.3203;
			if (moneda1.equals("Estados Unidos - Dólar")) 
				proporcion = 14650.00;
			if (moneda1.equals("Dinamarca - Corona")) 
				proporcion = 2320.0938;
			if (moneda1.equals("Brasil - Real")) 
				proporcion = 2608.2467;
			break;
		case "Moldavia - Leu":
			if (moneda1.equals("Moldavia - Leu")) 
				proporcion = 1.0;
			if (moneda1.equals("Argentina - Peso")) 
				proporcion = 0.2238;
			if (moneda1.equals("República Dominicana - Peso")) 
				proporcion = 0.2835;
			if (moneda1.equals("Reino Unido - Libras")) 
				proporcion = 21.8084;
			if (moneda1.equals("Indonesia - Rupia")) 
				proporcion = 0.001129;
			if (moneda1.equals("Europa - Euro")) 
				proporcion = 19.5072;
			if (moneda1.equals("Etiopía - Birr"))
				proporcion = 0.4589;
			if (moneda1.equals("Estados Unidos - Dólar"))
				proporcion = 16.546;
			if (moneda1.equals("Dinamarca - Corona")) 
				proporcion = 2.6204;
			if (moneda1.equals("Brasil - Real")) 
				proporcion = 2.9458;
			break;
		case "Reino Unido - Libras":
			if (moneda1.equals("Reino Unido - Libras")) 
				proporcion = 1.0;
			if (moneda1.equals("Argentina - Peso")) 
				proporcion = 0.01026;
			if (moneda1.equals("República Dominicana - Peso")) 
				proporcion = 0.013;
			if (moneda1.equals("Moldavia - Leu")) 
				proporcion = 0.04585;
			if (moneda1.equals("Indonesia - Rupia")) 
				proporcion = 0.00005179;
			if (moneda1.equals("Europa - Euro")) 
				proporcion = 0.8945;
			if (moneda1.equals("Etiopía - Birr")) 
				proporcion = 0.02104;
			if (moneda1.equals("Estados Unidos - Dólar")) 
				proporcion = 0.7587;
			if (moneda1.equals("Dinamarca - Corona")) 
				proporcion = 0.1202;
			if (moneda1.equals("Brasil - Real")) 
				proporcion = 0.1351;
			break;
		case "República Dominicana - Peso":
			if (moneda1.equals("República Dominicana - Peso")) 
				proporcion = 1.0;
			if (moneda1.equals("Argentina - Peso")) 
				proporcion = 0.7895;
			if (moneda1.equals("Reino Unido - Libras")) 
				proporcion = 76.9342;
			if (moneda1.equals("Moldavia - Leu")) 
				proporcion = 3.5277;
			if (moneda1.equals("Indonesia - Rupia")) 
				proporcion = 0.003984;
			if (moneda1.equals("Europa - Euro")) 
				proporcion = 68.8163;
			if (moneda1.equals("Etiopía - Birr")) 
				proporcion = 1.6189;
			if (moneda1.equals("Estados Unidos - Dólar")) 
				proporcion = 58.37;
			if (moneda1.equals("Dinamarca - Corona")) 
				proporcion = 9.244;
			if (moneda1.equals("Brasil - Real")) 
				proporcion = 10.392;
			break;
    }
		
	}
}